
public class Clientes {
	private String nombre;
	private String apellido;
	private String mail;
	private float monto;
	private int dni;
	private int nrocuenta;
	
	public Clientes(String nombre, String apellido, String mail, float monto, int dni, int nrocuenta) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.mail = mail;
		this.monto = monto;
		this.dni = dni;
		this.nrocuenta = nrocuenta;
	}
	public int getNrocuenta() {
		return nrocuenta;
	}
	public void setNrocuenta(int nrocuenta) {
		this.nrocuenta = nrocuenta;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public float getMonto() {
		return monto;
	}
	public void setMonto(float monto) {
		this.monto = monto;
	}
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	
		
}
